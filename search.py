import mmap
import sys
import time
import re
import logging

LOG_FILENAME = 'search.log'
logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG)


class DataStore:
	def __init__(self):
		start_time = time.time()
		file = open('list_of_hashes.txt', 'r')
		self.data = mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
		logging.debug("Total memory consumed: %s" % (sys.getsizeof(self.data)))

	def find(self, key):
		if self.data:
			regex = r"^%s\n" % re.escape(key)
			m = re.findall(regex, self.data, re.MULTILINE)
			return True if m else False
		return False


def main():
	store = DataStore()
	queries = int(raw_input())
	
	for q0 in xrange(queries):
		key = raw_input().strip()
		start_time = time.time()
		print("Found (%s): %s" % (key, store.find(key)))
		logging.debug("Search time: %s" % (time.time() - start_time))


if __name__ == '__main__':
	main()