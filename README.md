Description:
------------

Search a string in large file. If it founds it returns "true" else "false".


Requisites:
-----------

python2.x										


Input format:
------------

The first line needs to be an integer, denoting number of queries.
Subsequent lines are keys needs to be searched.

Example:

3

de7495bdc31212e6f6fa3c8bf466417

e6bc3fc3e95382909d664b352c51795

2645effdf8b83645920a6fc7b5e302d


If you wants, you can use the input file as well and run like following:

cat input | python search.py

Log:
---

It also creates the log file, it contains size of the object and time taken to perform each action.